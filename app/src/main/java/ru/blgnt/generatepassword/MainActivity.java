package ru.blgnt.generatepassword;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{

    private TextView password, count;

    private SeekBar mSeekBar;
    private CheckBox lowCaseCheckBox, upperCaseCheckBox, symbCheckBox, numbCheckBox;

    public static final int ABC_LENGT = 26;
    public static final int NUMBERS_LENGTH = 10;
    public static final int SYMBOLS_LENGTH = 21;

    private char [] abc =     { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                                'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                                'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                                'y', 'z'};

    private char [] ABC =     { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z'};

    private char [] numbers = { '0', '1', '2', '3', '4', '5', '6', '7',
                                '8', '9'};

    private char [] symbols = { '!', '@', '#', '$', '%', '^', '&', '*',
                                '(', ')', '-', '+', '=', ';', ':', ',',
                                '?', '[', ']', '{', '}'};

    public static final int abc_LIST = 0;
    public static final int ABC_LIST = 1;
    public static final int NUMBERS_LIST = 2;
    public static final int SYMBOLS_LIST = 3;

    private ArrayList<Integer> integerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        password = (TextView) findViewById(R.id.password);
        count = (TextView) findViewById(R.id.count);

        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
        mSeekBar.setOnSeekBarChangeListener(this);

        lowCaseCheckBox = (CheckBox) findViewById(R.id.lowCase);
        upperCaseCheckBox = (CheckBox) findViewById(R.id.upperCase);
        symbCheckBox = (CheckBox) findViewById(R.id.symb);
        numbCheckBox = (CheckBox) findViewById(R.id.numb);

    }

    public void generateButton(View view) {

        int CHECKED_SEEKBAR_COUNT = mSeekBar.getProgress();

        password.setText(generate_pass(CHECKED_SEEKBAR_COUNT));
    }


    /* 1. Создаем массив (list) на заданное количество символов
       2. Создаём список (characterList) и добавляем туда рандомные символы
       3. Перемешиваем список - метод shuffle
       4. Копируем список в массив characterList -> list
       5. Создаем из массива строку: list -> result
     */
    public String generate_pass(int count){

        char [] list = new char[count];
        integerList = randomLists(count);
        ArrayList<Character> characterList = creationPassword(count);

        Collections.shuffle(characterList);
        for (int i = 0; i < characterList.size(); i++){
            list[i] = characterList.get(i);
        }
        return new String(list);
    }

    /*
        Методы возвращают случайный символ из заданного массива символов
     */
    public char getLowerCaseAbs(){
        Random random = new Random();
        int i = random.nextInt(ABC_LENGT);
        char c = abc[i];
        return c;
    }

    public char getUpperCaseAbs(){
        Random random = new Random();
        int i = random.nextInt(ABC_LENGT);
        char c = ABC[i];
        return c;
    }

    public char getNumber(){
        Random random = new Random();
        int i = random.nextInt(NUMBERS_LENGTH);
        char c = numbers[i];
        return c;
    }

    public char getSymbol(){
        Random random = new Random();
        int i = random.nextInt(SYMBOLS_LENGTH);
        char c = symbols[i];
        return c;
    }

    /*
    Метод добавляет в список с определенным количеством символом, ограниченных int count
    случайные символы
     */
    public ArrayList<Character> creationPassword (int count){

        ArrayList<Character> list = new ArrayList<>();

        for (int i = 0; i < count; i++){

            if (integerList.get(i) == abc_LIST){
                list.add(getLowerCaseAbs());
            } else if (integerList.get(i) == ABC_LIST){
                list.add(getUpperCaseAbs());
            } else if (integerList.get(i) == NUMBERS_LIST){
                list.add(getNumber());
            } else if (integerList.get(i) == SYMBOLS_LIST){
                list.add(getSymbol());
            }
        }
        return  list;
    }

    /*
        Метод копирует текст в буфер обмена
     */

    public void copyToBuffer(View view){
        String pass = password.getText().toString();
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("pass", pass);
        clipboard.setPrimaryClip(clip);
        Toast toast = Toast.makeText(getApplicationContext(), "Текс скопирован", Toast.LENGTH_SHORT);
        toast.show();
    }

    /*

    Метод отслеживающий изменения в SeekBar и передающй значение в
    TextView count
     */

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        count.setText(String.valueOf(seekBar.getProgress()));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    /* Метод для генерации определенного количества чисел
       в случайной последовательности от 0 до 3 каждая.

       int r = check_abc_SYMB[random.nextInt(check_abc_SYMB.length)];

                if (r == abc_LIST){
                    list.add(check_abc[0]);
                }
                if (r == SYMBOLS_LIST){
                    list.add(check_SYMB[0]);
                }

      В переменную r заносится случайное число из массива check_abc_SYMB.
      Индекс вычисляется таким образом: вычисляется случайный индекс из длинны массива.
      Например, длинна массива check_abc_SYMB равна 2.
      Выбирается случайное число. Метод класса Random nextInte(count) Выбирает из чисел
      от 0 до count, где count не включается. Получаем, что при длинне массива 2
      метод выдаст нам значение либо 0, либо 1, то есть полностью удовлетворяющая нашему
      условию.
      r получит либо check_abc_SYMB[0], либо check_abc_SYMB[1].
        Далее мы проверяем, если r == abc_LIST, то есть 0 (например), то
        в список list добавляется 0, или другое, по условию.
     */

    public ArrayList<Integer> randomLists(int k){
        ArrayList<Integer> list = new ArrayList<>();
        Random random = new Random();

        /*
        Проверка одного чекбокса
         */
        if (                    lowCaseCheckBox.isChecked()    &&
                    !upperCaseCheckBox.isChecked()     &&
                    !numbCheckBox.isChecked()          &&
                    !symbCheckBox.isChecked()){

            for (int i = 0; i < k; i++){
                list.add(abc_LIST);
            }
        } else if   (!lowCaseCheckBox.isChecked()     &&
                                upperCaseCheckBox.isChecked() &&
                     !numbCheckBox.isChecked()        &&
                     !symbCheckBox.isChecked()){
            for (int i = 0; i < k; i++){
                list.add(ABC_LIST);
            }
        } else if   (!lowCaseCheckBox.isChecked()     &&
                     !upperCaseCheckBox.isChecked()   &&
                                numbCheckBox.isChecked()      &&
                     !symbCheckBox.isChecked()){
            for (int i = 0; i < k; i++){
                list.add(NUMBERS_LIST);
            }
        } else if   (!lowCaseCheckBox.isChecked()     &&
                     !upperCaseCheckBox.isChecked()   &&
                     !numbCheckBox.isChecked()        &&
                                symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {
                list.add(SYMBOLS_LIST);
            }

        /*
        Проверка двух нажатых чекбоксов
         */
        } else if   (           lowCaseCheckBox.isChecked()     &&
                                upperCaseCheckBox.isChecked()   &&
                     !numbCheckBox.isChecked()        &&
                     !symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {
                int r = check_abc_ABC[random.nextInt(check_abc_ABC.length)];

                if (r == abc_LIST){
                    list.add(abc_LIST);
                }
                if (r == ABC_LIST){
                    list.add(ABC_LIST);
                }

            }
        } else if   (           lowCaseCheckBox.isChecked()     &&
                !upperCaseCheckBox.isChecked()   &&
                                numbCheckBox.isChecked()        &&
                !symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {
                int r = check_abc_NUMB[random.nextInt(check_abc_NUMB.length)];

                if (r == abc_LIST){
                    list.add(abc_LIST);
                }
                if (r == NUMBERS_LIST){
                    list.add(NUMBERS_LIST);
                }

            }
        } else if   (           lowCaseCheckBox.isChecked()     &&
                !upperCaseCheckBox.isChecked()   &&
                !numbCheckBox.isChecked()        &&
                                symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {

                int r = check_abc_SYMB[random.nextInt(check_abc_SYMB.length)];

                if (r == abc_LIST){
                    list.add(abc_LIST);
                }
                if (r == SYMBOLS_LIST){
                    list.add(SYMBOLS_LIST);
                }

            }
        } else if (!lowCaseCheckBox.isChecked()     &&
                                upperCaseCheckBox.isChecked()   &&
                                numbCheckBox.isChecked()        &&
                   !symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {

                int r = check_ABC_NUMB[random.nextInt(check_ABC_NUMB.length)];

                if (r == ABC_LIST){
                    list.add(ABC_LIST);
                }
                if (r == NUMBERS_LIST){
                    list.add(NUMBERS_LIST);
                }

            }
        } else if (!lowCaseCheckBox.isChecked()     &&
                                upperCaseCheckBox.isChecked()   &&
                !numbCheckBox.isChecked()        &&
                                symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {

                int r = check_ABC_SYMB[random.nextInt(check_ABC_SYMB.length)];

                if (r == ABC_LIST){
                    list.add(ABC_LIST);
                }
                if (r == SYMBOLS_LIST){
                    list.add(SYMBOLS_LIST);
                }

            }
        } else if (!lowCaseCheckBox.isChecked()     &&
                !upperCaseCheckBox.isChecked()   &&
                                numbCheckBox.isChecked()        &&
                                symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {

                int r = check_NUMB_SYMB[random.nextInt(check_NUMB_SYMB.length)];

                if (r == NUMBERS_LIST){
                    list.add(NUMBERS_LIST);
                }
                if (r == SYMBOLS_LIST){
                    list.add(SYMBOLS_LIST);
                }

            }
        }
        /*
        Проверка трех нажатых чекбоксов
         */
        else if (               lowCaseCheckBox.isChecked()     &&
                                upperCaseCheckBox.isChecked()   &&
                                numbCheckBox.isChecked()        &&
                !symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {
                int r = check_abc_ABC_NUMB[random.nextInt(check_abc_ABC_NUMB.length)];

                if (r == abc_LIST){
                    list.add(abc_LIST);
                }
                if (r == ABC_LIST){
                    list.add(ABC_LIST);
                }
                if (r == NUMBERS_LIST){
                    list.add(NUMBERS_LIST);
                }
            }
        } else if (               lowCaseCheckBox.isChecked()     &&
                                  upperCaseCheckBox.isChecked()   &&
                !numbCheckBox.isChecked()        &&
                                  symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {
                int r = check_abc_ABC_SYMB[random.nextInt(check_abc_ABC_SYMB.length)];

                if (r == abc_LIST){
                    list.add(abc_LIST);
                }
                if (r == ABC_LIST){
                    list.add(ABC_LIST);
                }
                if (r == SYMBOLS_LIST){
                    list.add(SYMBOLS_LIST);
                }
            }
        } else if (!lowCaseCheckBox.isChecked()     &&
                                   upperCaseCheckBox.isChecked()   &&
                                   numbCheckBox.isChecked()        &&
                                   symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {
                int r = check_ABC_NUMB_SYMB[random.nextInt(check_ABC_NUMB_SYMB.length)];

                if (r == ABC_LIST) {
                    list.add(ABC_LIST);
                }
                if (r == NUMBERS_LIST) {
                    list.add(NUMBERS_LIST);
                }
                if (r == SYMBOLS_LIST) {
                    list.add(SYMBOLS_LIST);
                }
            }
        } else if (             lowCaseCheckBox.isChecked()     &&
                !upperCaseCheckBox.isChecked()   &&
                                numbCheckBox.isChecked()        &&
                                symbCheckBox.isChecked()) {
            for (int i = 0; i < k; i++) {
                int r = check_abc_NUM_SYMB[random.nextInt(check_abc_NUM_SYMB.length)];

                if (r == abc_LIST){
                    list.add(abc_LIST);
                }
                if (r == NUMBERS_LIST){
                    list.add(NUMBERS_LIST);
                }
                if (r == SYMBOLS_LIST){
                    list.add(SYMBOLS_LIST);
                }
            }

        /*
        Если все чекбоксы нажаты
         */
        } else {
            for (int i = 0; i < k; i++){
                list.add(random.nextInt(4));
            }
        }

        return list;
    }

    private int [] check_abc_ABC  = { 0, 1 };
    private int [] check_abc_NUMB = { 0, 2 };
    private int [] check_abc_SYMB = { 0, 3 };
    private int [] check_ABC_NUMB = { 1, 2 };
    private int [] check_ABC_SYMB = { 1, 3 };
    private int [] check_NUMB_SYMB = { 2, 3 };

    private int [] check_abc_ABC_NUMB = { 0, 1, 2 };
    private int [] check_abc_ABC_SYMB = { 0, 1, 3 };
    private int [] check_abc_NUM_SYMB = { 0, 2, 3 };
    private int [] check_ABC_NUMB_SYMB = { 1, 2, 3 };



}
